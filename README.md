# GCMV example - Pages

This is an example project that uses [GCMV](https://gitlab.com/lpireyn/gcmv)
to aggregate in the GitLab pages the documentation generated for each version.
